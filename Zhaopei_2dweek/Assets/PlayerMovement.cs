﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayerMovement : MonoBehaviour
{
    // Start is called before the first frame update

    Rigidbody2D rB2D;
    public float runSpeed;
    public float jumpForce;
    public SpriteRenderer spriteRenderer;
    public Animator animator;
    private int count;
    public TextMeshProUGUI countText;
    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();
        count = 0;
        

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            int levelMask = LayerMask.GetMask("Level");
            if(Physics2D.BoxCast(transform.position,new Vector2(1f,.1f),0f,Vector2.down,.01f,levelMask)){
                Jump();
            }
            
        }
    }

    private void FixedUpdate()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime, rB2D.velocity.y);

        if (rB2D.velocity.x > 0)
        {
            spriteRenderer.flipX = false;
        }else
            if (rB2D.velocity.x < 0)
        {
            spriteRenderer.flipX = true;
        }

        if (Mathf.Abs(horizontalInput) > 0f)
        {
            animator.SetBool("IsRunning", true);
        }
        else
        {
            animator.SetBool("IsRunning", false);
        }
    }


    void Jump()
    {
        rB2D.velocity = new Vector2(rB2D.velocity.x, jumpForce);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // ..and if the GameObject you intersect has the tag 'Pick Up' assigned to it..
        if (other.gameObject.CompareTag("Diamond"))
        {
            other.gameObject.SetActive(false);

            // Add one to the score variable 'count'
            count = count + 1;

            // Run the 'SetCountText()' function (see below)
      

            SetCountText();
        }
        if (other.gameObject.CompareTag("DeathZone"))
        {
             Scene run = SceneManager.GetActiveScene();
            SceneManager.LoadScene(run.name); 
        }
        void SetCountText()
            {
                countText.text = "Count: " + count.ToString();

               
            }
        } }
